
angular.module('curveypaths', []).

    directive('curveypaths', function() {
        return {
            restrict: 'A',
            template: "<g class=\"curvedpaths\"><path class=\"path {{path.class || '' }}\" ng-repeat=\"path in pathsdata\" d=\"{{path.d || '' }}\" stroke-width=\"{{path.stoke || '4px' }}\" stroke=\"{{path.color || '#000' }}\" stroke-linecap=\"{{path.cap || 'butt' }}\" stroke-dasharray=\"{{path.dash || '' }}\" fill=\"{{path.fill || 'transparent' }}\"/></g>"
        };
    }).

    provider('$curveypaths', ['$injector', function()
    {
        // constructor
        this.$get = function ( $injector )
        {
            $rootScope = $injector.get ('$rootScope');

            if ($rootScope.pathsdata == null)
                $rootScope.pathsdata = [];

            return this;
        };

        // draw
        this.draw = function ( argA, argB )
        {
            // overload : just an path object
            if (argB == null)
            {
                $rootScope.pathsdata.push (argA);
            }

            // overload : name and path object
            else
            {
                var pathId = argA;
                var pathNewData = argB;
                var pathData = this.getPathById (pathId);

                // if this id did not excist yet
                if (pathData == null)
                {
                    pathNewData.id = pathId;
                    $rootScope.pathsdata.push (pathNewData);
                }

                // otherwise, update it
                else
                {
                    for (var pathNewDataKey in pathNewData)
                    {
                        pathData[pathNewDataKey] = pathNewData[pathNewDataKey];
                    }
                }
            }

            this.apply();
        };

        // delete a path
        this.delete = function ( id )
        {
            if (id == null)
            {
                $rootScope.pathsdata = [];
            }
            else
            {
                for (var i = 0; i < $rootScope.pathsdata.length; i++)
                {
                    if ($rootScope.pathsdata[i].id == id)
                    {
                        $rootScope.pathsdata.splice (i, 1);
                    }
                }
            }
        }

        // get a path by id
        this.getPathById = function ( id )
        {
            for (var i = 0; i < $rootScope.pathsdata.length; i++)
            {
                if ($rootScope.pathsdata[i].id == id)
                {
                    return $rootScope.pathsdata[i];
                }
            }
            return null;
        };

        // apply paths to svg
        this.apply = function ( )
        {
            for (var i = 0; i < $rootScope.pathsdata.length; i ++)
            {
                // Ready to nerd? :D

                var _p = $rootScope.pathsdata[i];
                var _d_from_orgin_x = 0;
                var _d_from_orgin_y = 0;
                var _d_from_anchor_x = 0;
                var _d_from_anchor_y = 0;
                var _d_to_orgin_x = 0;
                var _d_to_orgin_y = 0;
                var _d_to_anchor_x = 0;
                var _d_to_anchor_y = 0;
                var _d = "";

                var _c_distance = 0;

                _d_from_orgin_x = _p.fromX;
                _d_from_orgin_y = _p.fromY;

                _d_to_orgin_x = _p.toX;
                _d_to_orgin_y = _p.toY;

                _c_vertical_distance = ( _d_from_orgin_x > _d_to_orgin_x) ? ( _d_from_orgin_x - _d_to_orgin_x ) : ( _d_to_orgin_x - _d_from_orgin_x );
                _c_horizontal_distance = ( _d_from_orgin_y > _d_to_orgin_y) ? ( _d_from_orgin_y - _d_to_orgin_y ) : ( _d_to_orgin_y - _d_from_orgin_y );

                if (_c_vertical_distance < 200) _c_vertical_distance = 200;
                if (_c_vertical_distance > 400) _c_vertical_distance = 400;

                if (_p.hookFrom == "left")
                    _d_from_anchor_x = _d_from_orgin_x - ( _c_vertical_distance / 2);
                else
                    _d_from_anchor_x = _d_from_orgin_x + ( _c_vertical_distance / 2);
                    
                if (_p.hookTo == "right")
                    _d_to_anchor_x = _d_to_orgin_x + ( _c_vertical_distance / 2);
                else
                    _d_to_anchor_x = _d_to_orgin_x - ( _c_vertical_distance / 2);

                _d_from_anchor_y = _d_from_orgin_y;
                _d_to_anchor_y = _d_to_orgin_y;

                _d = "M" + _d_from_orgin_x + "," + _d_from_orgin_y + " C" + _d_from_anchor_x + "," + _d_from_anchor_y + " " + _d_to_anchor_x + "," + _d_to_anchor_y + " " + _d_to_orgin_x + "," + _d_to_orgin_y;

                $rootScope.pathsdata[i].d = this.validateD (_d);
            }
        }

        // validate a generated d element
        this.validateD = function ( _d )
        {
            if ( (_d.toLowerCase().indexOf ("nan") != -1) && (_d.toLowerCase().indexOf ("undefined") != -1) )
            {
                return ""; 
            }
            else
            {
                return _d;
            }
        };
    }]);