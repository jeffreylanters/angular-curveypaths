![alt tag](https://dev.jeffreylanters.nl/qimg/curvedpaths-1.png)

# angular-curveypaths
Curvey paths is a package that contains a angular 1.x module for drawing node-like paths to for example connect elements and create visual envoirments.

Note that this package is meant to be used for `npm` only.

## Installation
First install the package using npm.
```sh
npm install angular-curveypaths
```
Once installed succesfully, add the `curveypaths` module to your Angular application.
```javascript
angular.module('app', ['curveypaths']);
```

## Example
Example of the most basic usage of curved paths

![alt tag](https://dev.jeffreylanters.nl/qimg/curvedpaths-2.png)

## Usage
The first thing you want to do is create a svg element within your applications HTML file. Let me do the math!
> NOTE: Since the curveypaths svg canvas is transparent, you can simple lay it over your excisting application. Any style attributes may be applied to this svg canvas.
```html
<svg curveypaths xmlns="http://www.w3.org/2000/svg" version="1.1"> </svg>
```
You can access the curvey paths from anywhere in your application, simply use the `$curveypaths` provider and you're ready to go.
```javascript
// for example in the run ...
.run (['$rootScope', '$curveypaths', function ($rootScope, $curveypaths) {
// or in a controller!
.controller('myController', ['$scope', '$curveypaths', function ($scope, $curveypaths) {
```

## Drawing paths
To draw a path, simple use the `$curveypaths.draw ();`.
> NOTE: You can pass a option path name. When you draw a path with a name, you can use the excact same function to update and redraw this path. Only the pathData you pass will be updated! (So for example the color or end position). When you don't pass a path name, your path will be drawn once and permanent and will no longer be editable.
```javascript
// to draw a permanent path.
$curveypaths.draw ( object : pathData );
// to draw a dynamic path.
$curveypaths.draw ( string : pathName, object : pathData );
```

### PathData object
> TIP: You can make the path hook on from the left or from the right using the hook attribute. Paths can also be inverted using negative a negative (or lower that the start) end position. Set relative to true to set the to position relative from the from position.
```javascript
$curveypaths.draw ('myPath',{
    // int : the path start X and Y
    fromX: 50,
    fromY: 50,
    // int : the path end X and Y
    toX: 350,
    toY: 400,
    // Optional (default false) bool : false or true
    relative: false,
    // Optional (default #000) color : hex, rgb or rgba
    color: '#000',
    // Optional (default transparent) color : hex, rgb or rgba
    fill: '#000',
    // Optional (default 4px) string : px, em, %
    stoke: '10px',
    // Optional (default butt) string : 'butt', 'round' or 'square'
    cap: 'butt',
    // Optional (default 0) string : dash array
    dash: '1,5',
    // Optional (default left) string : 'left' or 'right'
    hookFrom: 'right',
    hookTo: 'left',
    // Optional (default null) string : a custom class name
    class: 'aTest'
});
```
If you want to update any of the given attributes, you can simple use the draw command with only the attributes you want to update using the same path name.
```javascript
$curveypaths.draw ('myPath',{
    toX: 100,
    color: '#ff0'
});
```

### Removing paths
To remove a specific path, simply use the remove function and pass the path name.
```javascript
$curveypaths.remove ('myPath');
```
To remove all paths, just use remove.
```javascript
$curveypaths.remove ();
```

### Styling paths
Besides passing the styling attributes within the pathData and adding a custom class, you can style paths in general by overwriting the base class along with custom tags.
```scss
.curvedpaths .path {
    fill: red;
    &.aTest {
        fill: green;
    }
}
```